import _ from 'lodash'
export const fetchCurrencies = async () => {
    const baseURL = 'https://free.currconv.com/api/v7/convert?q=';
    const apiKey = _.sample(['43011bbe2634d701dff0', 'c84aa5c28114885a2d28', 'f713085512637600ea3c', '21bf527bae2f747a094a', '1bbe5824067ca32fcad7']);
    return { // &apiKey=${apiKey}
        ILS: {
            EUR: await fetch(`${baseURL}EUR_ILS&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.EUR_ILS.val),
            HRK: await fetch(`${baseURL}HRK_ILS&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.HRK_ILS.val),
            USD: await fetch(`${baseURL}USD_ILS&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.USD_ILS.val)
        }, 

        EUR: {
            ILS: await fetch(`${baseURL}ILS_EUR&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.ILS_EUR.val),
            HRK: await fetch(`${baseURL}HRK_EUR&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.HRK_EUR.val),
            USD: await fetch(`${baseURL}USD_EUR&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.USD_EUR.val)
        }, 

        HRK: {
            EUR: await fetch(`${baseURL}EUR_HRK&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.EUR_HRK.val),
            ILS: await fetch(`${baseURL}ILS_HRK&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.ILS_HRK.val),
            USD: await fetch(`${baseURL}USD_HRK&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.USD_HRK.val)
        },
        USD: {
            ILS: await fetch(`${baseURL}ILS_USD&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.ILS_USD.val),
            EUR: await fetch(`${baseURL}EUR_USD&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.EUR_USD.val),
            HRK: await fetch(`${baseURL}HRK_USD&compact=y&apiKey=${apiKey}`).then(res => res.json()).then(res => res.HRK_USD.val)
        },
    }
}